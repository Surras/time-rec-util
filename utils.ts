import { Config, Record } from './types';
import { promisify } from 'util';
import fs from 'fs';
import { DateTime } from 'luxon';

export async function readConfig(): Promise<Config> {
  return JSON.parse(await promisify(fs.readFile)('config.json', 'utf8'));
}

export async function readTimeRecords(path: string): Promise<Array<Record>> {
  const logs: Array<Record> = await JSON.parse(await promisify(fs.readFile)(path, 'utf8'));
  return logs.map(log => {
    const duration = DateTime.fromISO(log.endTime).diff(DateTime.fromISO(log.startTime)).as('minutes');
    const splitted = log.remarks?.split('#');
    let ticket = null;
    let remarks = null;
    if (splitted) {
      ticket = splitted[0].trim();
      remarks = splitted[1]?.trim() || '';
    }

    return {
      bookingId: log.bookingId,
      startTime: log.startTime,
      endTime: log.endTime,
      duration,
      ticket,
      remarks,
    } as Record;
  });
}
import { readConfig, readTimeRecords } from './utils';
import { DateTime } from 'luxon';
import xlsxPopulate from 'xlsx-populate';
import { fetchTicketTitle } from './jira-utils';
import { Record } from './types';

interface ExcelRow {
  date: DateTime,
  ticket?: string,
  duration?: number,
  description?: string
}

export async function createExcel(inputFile: string) {
  const config = await readConfig();
  const records = (await readTimeRecords(inputFile))
    .sort((a, b) => new Date(a.startTime).getTime() - new Date(b.startTime).getTime());
  let excelRows: Array<ExcelRow> = [];
  const firstTimeStamp = DateTime.fromISO(records[0].startTime);
  console.log('records to export:', records.length);

  for (let day = 1; day <= firstTimeStamp.daysInMonth; day++) {
    const curDate = firstTimeStamp.set({ day, hour: 0, minute: 0, second: 0, millisecond: 0 });
    const logs = records.filter(record => DateTime.fromISO(record.startTime).set({
        hour: 0,
        minute: 0,
        second: 0,
        millisecond: 0,
      }).toISO() === curDate.toISO(),
    );

    try {
      if (logs.length) {

        // group entries by tickets
        const groupedLogs = logs.reduce((prev, cur) => {
          let existing = prev.find(i => i.ticket === cur.ticket && i.remarks === cur.remarks);
          if (existing) {
            existing.duration = existing.duration + cur.duration;
          } else {
            prev.push(cur);
          }
          return prev;
        }, [] as Array<Record>);

        const mappedRows: Array<ExcelRow> = groupedLogs.map(record => ({
          date: DateTime.fromISO(record.startTime),
          ticket: record.ticket,
          duration: convertDuration(record.duration),
          description: record.remarks,
        }));

        // refine rows
        for (let row of mappedRows) {
          if ((!row.description || !row.description.length) && row.ticket) {
            console.log(`fetch description for ticket ${row.ticket}...`);
            try {
              row.description = await fetchTicketTitle(row.ticket);
            } catch (err) {
              console.error('error while fetch ticket description:', err);
              row.description = 'SERVER FETCH ERROR';
            }
          }

          row.ticket = `${config.ticketPrefix}-${row.ticket}`;
        }

        excelRows = [...excelRows, ...mappedRows];
      } else {
        // add empty row
        excelRows.push({ date: curDate });
      }
    } catch (e) {
      console.error('error', e);
    }
  }

  // write to excel
  const workbook = await xlsxPopulate.fromFileAsync(config.excel.templateFile);
  const sheet = workbook.sheet(config.excel.templateSheet);

  // edit sheet name first to current month and year
  sheet.name(firstTimeStamp.toFormat('MMMM yyyy', { locale: 'de-DE' }));

  const timestampCol = config.excel.timestampCol;
  const durationCol = config.excel.durationCol;
  const ticketCol = config.excel.ticketCol;
  const noteCol = config.excel.noteCol;
  const excelExportFileName = config.excel.exportFileNamePrefix;

  let currentRow = config.excel.startRow;

  sheet.cell(config.excel.summaryDateCell).value(new Date(excelRows[0].date.year, excelRows[0].date.month - 1, excelRows[0].date.day));

  // write rows
  for (let row of excelRows) {
    sheet.cell(`${timestampCol}${currentRow}`).value(new Date(row.date.year, row.date.month - 1, row.date.day));
    sheet.cell(`${durationCol}${currentRow}`).value(row.duration);
    sheet.cell(`${ticketCol}${currentRow}`).value(row.ticket);
    sheet.cell(`${noteCol}${currentRow}`).value(row.description);

    currentRow++;
  }

  await workbook.toFileAsync(`${firstTimeStamp.toFormat('yyyyMM')}${excelExportFileName}.xlsx`);
}

function convertDuration(duration: number) {
  const rounded = (Math.round(duration / 15) * 15);
  return Math.floor(rounded / 60) + ((rounded % 60) / 60);
}
import {Config, JiraTicketResponse, Record} from './types';
import axios from 'axios';
import https from 'https';
import {readConfig} from './utils';
import {DateTime} from 'luxon';

const httpsAgent = new https.Agent({
  rejectUnauthorized: false,
});

export async function fetchTicketTitle(
  ticket: string,
): Promise<string> {
  const config = await readConfig();
  const response = await axios.get<JiraTicketResponse>(
    `${config.baseUrl}/rest/api/2/issue/${config.ticketPrefix}-${ticket}`,
    {
      auth: {username: config.username, password: config.password},
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      httpsAgent,
    },
  );

  if (response) {
    return response.data.fields.summary;
  } else {
    return '';
  }
}

export async function uploadWorkLog(
  record: Record,
) {
  const config = await readConfig();

  const minutes = (Math.round(record.duration / 15) * 15);
  const date = DateTime.fromJSDate(new Date(record.startTime));

  return axios.post(
    `${config.baseUrl}/rest/api/2/issue/MAR-${record.ticket}/worklog`,
    {
      started: `${date.toISO({format: 'extended', includeOffset: false})}+0200`,
      timeSpent: `${minutes}m`,
      comment: record.remarks,
    },
    {
      auth: {username: config.username, password: config.password},
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      httpsAgent,
    },
  );
}

import * as yargs from 'yargs';
import { createExcel } from './create-excel';
import fs from 'fs';
import path from 'path';
import { uploadWorkLog } from './jira-utils';
import { readTimeRecords } from './utils';
import { Record } from './types';
import { writeFile } from 'fs/promises';

yargs
  .usage('$0 <cmd> [args]')
  .command(
    'export [input]',
    'fetch ticket titles and export to excel file',
    (args) =>
      args.positional('input', {
        type: 'string',
        describe: 'input JSON file with records',
        demandOption: true,
      }),
    (args) => {
      exportToExcel(args.input)
        .then(() => console.log('...export finished'))
        .catch((err) => console.error('error while export to excel:', err));
    },
  )
  .command(
    'upload [input]',
    'upload records to JIRA workLog',
    (args) =>
      args
        .positional('input', {
          type: 'string',
          describe: 'input JSON file with records',
          demandOption: true,
        })
        .positional('start', {
          type: 'number',
          describe: 'ID of records in input file to start from',
        })
        .positional('single', {
          type: 'number',
          describe: 'single ID to upload',
        }),
    (args) => {
      uploadToJiraWorkLog(args.input, args.start, args.single)
        .then(() => console.log('... upload finished'))
        .catch((err) =>
          console.error('error while upload to jira:', err.message),
        );
    },
  )
  .command(
    'sort [input]',
    'sort given records file',
    (args) =>
      args.positional('input', {
        type: 'string',
        describe: 'input JSON file with records',
        demandOption: true,
      }),
    (args) => {
      sortRecordFile(args.input);
    },
  )
  .help().argv;

async function sortRecordFile(inputFile: string) {
  const fileRecords = await readTimeRecords(inputFile);
  fileRecords.sort((curr, prev) => curr.bookingId - prev.bookingId);

  console.log('parsed', path.parse(inputFile).base);
  await writeFile(
    `${path.parse(inputFile).name}_sorted.json`,
    JSON.stringify(fileRecords),
  );

  //fileRecords.map(record => `booking-ID: ${record.bookingId}`).forEach(v => console.log(v));
}

async function exportToExcel(inputFile: string) {
  // check input file
  try {
    if (!inputFile || path.parse(inputFile).ext !== '.json') {
      console.log('please provide a JSON file');
      process.exit(100);
    }

    await fs.promises.stat(inputFile);
  } catch (err) {
    console.log(`${inputFile} does not exist`);
    process.exit(100);
  }

  return await createExcel(inputFile);
}

async function uploadToJiraWorkLog(
  inputFile: string,
  startId?: number,
  singleId?: number,
) {
  const fileRecords = await readTimeRecords(inputFile);
  let records: Array<Record> = [];

  if (singleId !== undefined) {
    console.log(`looking for record with ID ${singleId}`);
    // const rec = fileRecords.find(r => r.id === singleId);
    // if (rec) {
    //     records.push(rec);
    // } else {
    //     console.log(`Record with ID ${singleId} in file ${inputFile} not found`);
    // }
  } else if (startId !== undefined) {
    console.log(`grab all records start from ID ${startId}`);
    // records = records.concat(fileRecords.filter(r => r.id >= startId));
  } else {
    records = records.concat(fileRecords);
  }
  console.log(`${records.length} records to upload`);
  for (let record of records) {
    console.log(`ID ${record.bookingId} -- ticket: ${record.ticket}...`);
    await uploadWorkLog(record);
  }
}

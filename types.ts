export interface Config {
    baseUrl: string;
    username: string;
    password: string;
    meetingTicket: string;
    frontendDailyTicket: string;
    backendDailyTicket: string;
    frontendDailyDefaultText: string;
    backendDailyDefaultText: string;
    ticketPrefix: string;
    excel: {
        startRow: number,
        timestampCol: string,
        durationCol: string,
        ticketCol: string,
        noteCol: string,
        summaryDateCell: string,
        templateSheet: string,
        exportFileNamePrefix: string,
        templateFile: string
    }
}

export interface Record {
    bookingId: number;
    startTime: string;
    endTime: string;
    ticket: string;
    duration: number;
    remarks?: string;
}

export interface JiraTicketResponse {
    fields: { summary: string }
}